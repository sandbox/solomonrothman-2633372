<?php
/**
 * @file Main file for Remove Empty Filter Module
 */

namespace Drupal\remove_empty_filter\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Class RemoveEmptyFilter
 * @package Drupal\remove_empty_filter\Plugin\Filter
 *
 * @Filter(
 *   id = "filter_remove_empty",
 *   title = @Translation("Remove Empty Tags Filter"),
 *   description = @Translation("Removes empty html tags, including those containing only &nbsp from content for improved display and accessibility"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */

class RemoveEmptyFilter extends FilterBase{

  public function process($text, $langcode){
    // regex to find and replace empty tags with nothing
    $text = preg_replace('/<[^\/>][^>]*>(|&nbsp;|\s)<\/[^>]+>/', '', $text);
    // Logs an error
 //   \Drupal::logger('remove_empty_filter')->notice('<pre>' . print_r($text,TRUE) . '</pre>');
    return new FilterProcessResult($text);
  }

}